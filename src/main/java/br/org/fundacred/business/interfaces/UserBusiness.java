package br.org.fundacred.business.interfaces;

import br.org.fundacred.model.User;

public interface UserBusiness {

	User createUser(User user);

	User createdUser(User user, User newUser);

	String decodeLogin(String login);

}