package br.org.fundacred.business;

import java.time.Instant;
import java.util.Set;
import java.util.UUID;

import org.springframework.stereotype.Component;

import br.org.fundacred.business.interfaces.UserBusiness;
import br.org.fundacred.model.Phone;
import br.org.fundacred.model.User;
import br.org.fundacred.utis.UtilCodecBasic;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class UserBusinessImpl implements UserBusiness {

	@Override
	public User createUser(User user) {
		Instant now = Instant.now();
		return User.builder().name(user.getName()).email(user.getEmail())
				.password(UtilCodecBasic.encodeBase64String(user.getPassword())).phones(user.getPhones()).created(now)
				.modified(now).token(UUID.randomUUID().toString()).lastLogin(now).build();

	}

	@Override
	public User createdUser(User user, User newUser) {
		return user.withId(newUser.getId()).withCreated(newUser.getCreated()).withModified(newUser.getModified())
				.withLastLogin(newUser.getLastLogin()).withPhones(newUser.getPhones()).withToken(newUser.getToken());
	}

	@Override
	public String decodeLogin(String login) {
		return UtilCodecBasic.decodeBase64String(login);
	}

}