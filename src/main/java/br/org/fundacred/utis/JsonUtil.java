package br.org.fundacred.utis;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JsonUtil {

	public static final ObjectMapper mapper = new ObjectMapper();

	@SneakyThrows
	public static String toJsonPretty(Object object) {
		return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
	}

	@SneakyThrows
	public static String convertToJsonString(final Object obj) {
		return mapper.writeValueAsString(obj);
	}

}