package br.org.fundacred.utis;

import org.apache.commons.codec.binary.Base64;

public class UtilCodecBasic {

	public static String encodeBase64String(String decoded) {
		return new String(Base64.encodeBase64(decoded.getBytes()));
	}

	public static String decodeBase64String(String encoded) {
		return new String(Base64.decodeBase64(encoded.getBytes()));
	}

}