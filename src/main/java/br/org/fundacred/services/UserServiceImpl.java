package br.org.fundacred.services;

import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import br.org.fundacred.business.interfaces.UserBusiness;
import br.org.fundacred.interfaces.repositories.UserRepository;
import br.org.fundacred.interfaces.services.UserService;
import br.org.fundacred.model.User;
import lombok.RequiredArgsConstructor;

@Service
@Component
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;
	private final UserBusiness userBusiness;

	@Override
	public Optional<User> findUser(Long id) {
		return Optional.ofNullable(userRepository.findUserById(id));
	}

	@Override
	public List<User> findAll() {
		return StreamSupport
				.stream(Spliterators.spliteratorUnknownSize(userRepository.findAll().iterator(), Spliterator.ORDERED),
						false)
				.collect(Collectors.toList());
	}

	@Override
	public User createUser(User user) {
		User newUser = userBusiness.createUser(user);
		userRepository.saveUser(newUser);
		return userBusiness.createdUser(user, newUser);
	}

	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public User findByToken(String token) {
		return userRepository.findByToken(token);
	}

	@Override
	public User findUserByEmailAndPassword(String email, String password) {
		return userRepository.findUserByEmailAndPassword(email, password);
	}

	@Override
	public Optional<User> findById(Long id) {
		return userRepository.findById(id);
	}

}