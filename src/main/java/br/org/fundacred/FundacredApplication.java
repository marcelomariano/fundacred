package br.org.fundacred;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FundacredApplication {

	public static void main(String[] args) {
		SpringApplication.run(FundacredApplication.class, args);
	}

}
