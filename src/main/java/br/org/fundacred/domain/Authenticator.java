package br.org.fundacred.domain;

import java.io.Serializable;

import br.org.fundacred.utis.JsonUtil;
import lombok.Data;

@Data
public class Authenticator implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8533313663144476559L;
	private String email;
	private String password;

	@Override
	public String toString() {
		return JsonUtil.toJsonPretty(this);
	}
}
