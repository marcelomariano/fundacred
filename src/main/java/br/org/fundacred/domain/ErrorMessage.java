package br.org.fundacred.domain;

import java.io.Serializable;

import br.org.fundacred.enums.ErrorType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@Builder
@AllArgsConstructor
public class ErrorMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1226539279772381615L;

	private String message;

	public static ErrorMessage message(ErrorType errorType) {
		return ErrorMessage.builder().message(errorType.getDescription()).build();
	}

}