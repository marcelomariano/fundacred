package br.org.fundacred.enums;

import static br.org.fundacred.constants.Messages.EMAIL_ALREADY_EXIST_MSG;
import static br.org.fundacred.constants.Messages.INVALID_SESSION_MSG;
import static br.org.fundacred.constants.Messages.INVALID_USER_OR_PWD_MSG;
import static br.org.fundacred.constants.Messages.NOT_AUTORIZED_MSG;
import static br.org.fundacred.constants.Messages.USER_NOT_FOUND_MSG;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;;

@Getter
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
public enum ErrorType {

	USER_NOT_FOUND_MSG_TYPE(USER_NOT_FOUND_MSG) {
		@Override
		public String descricao() {
			return this.getDescription();
		}
	},
	EMAIL_ALREADY_EXIST_MSG_TYPE(EMAIL_ALREADY_EXIST_MSG) {
		@Override
		public String descricao() {
			return this.getDescription();
		}
	},
	INVALID_USER_OR_PWD_MSG_TYPE(INVALID_USER_OR_PWD_MSG) {
		@Override
		public String descricao() {
			return this.getDescription();
		}
	},
	NOT_AUTORIZED_MSG_TYPE(NOT_AUTORIZED_MSG) {
		@Override
		public String descricao() {
			return this.getDescription();
		}
	},
	INVALID_SESSION_MSG_TYPE(INVALID_SESSION_MSG) {
		@Override
		public String descricao() {
			return this.getDescription();
		}
	};
	private String description;
	public abstract String descricao();
}