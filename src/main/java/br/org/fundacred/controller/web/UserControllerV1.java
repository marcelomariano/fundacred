package br.org.fundacred.controller.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import br.org.fundacred.model.User;

public interface UserControllerV1 {

	User findUser(Long id);

	List<User> findAll();

	ResponseEntity<?> createUser(@Valid User user);

}