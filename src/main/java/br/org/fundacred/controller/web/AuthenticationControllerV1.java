package br.org.fundacred.controller.web;

import java.util.Map;

import org.springframework.http.ResponseEntity;

import br.org.fundacred.domain.Authenticator;

public interface AuthenticationControllerV1 {

	ResponseEntity<Object> login(Map<String, String> headers, Authenticator authenticator,Long id);

}
