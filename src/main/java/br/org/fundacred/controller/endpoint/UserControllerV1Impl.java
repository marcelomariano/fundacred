package br.org.fundacred.controller.endpoint;

import static br.org.fundacred.domain.ErrorMessage.message;

import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.org.fundacred.business.interfaces.UserBusiness;
import br.org.fundacred.constants.Messages;
import br.org.fundacred.controller.web.UserControllerV1;
import br.org.fundacred.enums.ErrorType;
import br.org.fundacred.interfaces.services.UserService;
import br.org.fundacred.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/v1/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserControllerV1Impl implements UserControllerV1 {

	private final UserService userService;
	private final UserBusiness userBusiness;

	@Override
	@SuppressWarnings("rawtypes")
	@PostMapping(value = "/cadastro")
	public ResponseEntity<?> createUser(@Valid @RequestBody User user) {
		log.info("User " + user + " register running ... ");
		if (Objects.nonNull(userService.findByEmail(user.getEmail()))) {
			return new ResponseEntity(message(ErrorType.EMAIL_ALREADY_EXIST_MSG_TYPE), HttpStatus.NOT_FOUND);
		}
		User newUser = userService.createUser(user);
		log.info("User " + newUser + " " + Messages.SUCESSFULLY_REGISTER_MSG);
		return new ResponseEntity(newUser, HttpStatus.OK);
	}

	@Override
	@GetMapping(value = "/{id}")
	public User findUser(@PathVariable("id") Long id) {
		return userService.findUser(id).map(user -> {
			return user.withPassword(userBusiness.decodeLogin(user.getPassword()));
		}).orElseThrow(() -> new RuntimeException("Usuario nao encontrado com id:[" + id + "]"));
	}

	@Override
	@GetMapping("all")
	public List<User> findAll() {
		return userService.findAll();
	}

}