package br.org.fundacred.controller.endpoint;

import static br.org.fundacred.domain.ErrorMessage.message;
import static br.org.fundacred.enums.ErrorType.INVALID_USER_OR_PWD_MSG_TYPE;
import static br.org.fundacred.enums.ErrorType.NOT_AUTORIZED_MSG_TYPE;

import java.util.Map;
import java.util.Objects;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.org.fundacred.constants.Messages;
import br.org.fundacred.controller.web.AuthenticationControllerV1;
import br.org.fundacred.domain.Authenticator;
import br.org.fundacred.interfaces.services.UserService;
import br.org.fundacred.model.User;
import br.org.fundacred.utis.UtilCodecBasic;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/v1/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthenticationControllerV1Impl implements AuthenticationControllerV1 {

	private final UserService userService;

	@Override
	@SneakyThrows
	@RequestMapping(value = "/login/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> login(@RequestHeader Map<String, String> headers,
			@RequestBody Authenticator authenticator, @PathVariable(required = true, value = "id") Long id) {
		log.info("AuthenticationControllerV1Impl: body - {}", authenticator);

		if (Objects.isNull(userService.findByEmail(authenticator.getEmail()))) {
			return notFound();
		} else {
			if (Objects.isNull(userService.findUserByEmailAndPassword(authenticator.getEmail(),
					UtilCodecBasic.encodeBase64String(authenticator.getPassword())))) {
				return notFound();
			} else {
				String token = headers.get("token");
				if (Objects.isNull(token) || token.isEmpty()) {
					return unautorized();
				} else {
					User findBytoken = userService.findByToken(token);
					if (Objects.isNull(findBytoken)) {
						return unautorized();
					} else {
						return userService.findById(id)
								.map(u -> {
										return new ResponseEntity(u, HttpStatus.OK);
								}).orElseThrow(() -> new RuntimeException(Messages.USER_NOT_FOUND_MSG));
					}
				}
			}
		}
	}

	private ResponseEntity<Object> notFound() {
		return new ResponseEntity(message(INVALID_USER_OR_PWD_MSG_TYPE), HttpStatus.NOT_FOUND);
	}

	private ResponseEntity<Object> unautorized() {
		return new ResponseEntity(message(NOT_AUTORIZED_MSG_TYPE), HttpStatus.UNAUTHORIZED);
	}

}