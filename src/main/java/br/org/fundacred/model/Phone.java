package br.org.fundacred.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import br.org.fundacred.utis.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@JsonInclude(Include.NON_NULL)
@Entity
@Table(name = "phones", uniqueConstraints = @UniqueConstraint(columnNames = { "ddd", "number" }))
public class Phone implements IPojo<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1007515770591382939L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long 	id;
	@Column(name = "ddd")
	@Size(
	  min = 2,
	  max = 3,
	  message = "The ddd '${validatedValue}' must be between {min} and {max} characters long"
	)
	private String 	ddd;
	@Column(name = "number")
	private String 	number;

	@Override
	public String toString() {
		return JsonUtil.toJsonPretty(this);
	}

}
