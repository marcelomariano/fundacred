package br.org.fundacred.model;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import br.org.fundacred.utis.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Wither;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@JsonInclude(Include.NON_NULL)
@Entity
@Table(name = "users")
public class User implements IPojo<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4329776702472576638L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Wither private Long 		id;
	@Column(name = "name")
	@NotNull(message = "O campo nome nao pode ser nulo")
	private String 		name;
	@Column(name = "email")
	@NotNull(message = "O campo email nao pode ser nulo")
	private String 		email;
	@Column(name = "password")
	@NotNull(message = "O campo password nao pode ser nulo")
	@Wither private String 		password;
	@OneToMany(targetEntity=Phone.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Builder.Default
	@Wither private Set<Phone> phones = new HashSet<>();

	@Column(name = "created")
	@Wither private Instant 	created;
	@Column(name = "modified")
	@Wither private Instant 	modified;
	@Column(name = "lastLogin")
	@Wither private Instant 	lastLogin;
	@Column(name = "token")
	@Wither private String 		token;

	@Override
	public String toString() {
		return JsonUtil.toJsonPretty(this);
	}

}