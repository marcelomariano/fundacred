package br.org.fundacred.constants;

import br.org.fundacred.exceptions.BusinessException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Messages {

	public static final String SUCESSFULLY_REGISTER_MSG = "Usuario cadastrado com sucesso";
	//errors-msg
	public static final String USER_NOT_FOUND_MSG 		= "Usuario nao encontrado";
	public static final String EMAIL_ALREADY_EXIST_MSG 	= "E-mail ja existente";
	public static final String INVALID_USER_OR_PWD_MSG 	= "Usuario e/ou senha invalidos";
	public static final String NOT_AUTORIZED_MSG 		= "Nao autorizado";
	public static final String INVALID_SESSION_MSG 		= "Sessao invalida";

	@SneakyThrows
	public static void validationException(String msg) {
		log.warn(msg);
		throw new BusinessException(msg);
	}

}