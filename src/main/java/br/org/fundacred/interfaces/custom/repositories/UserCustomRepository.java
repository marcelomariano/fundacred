package br.org.fundacred.interfaces.custom.repositories;

import br.org.fundacred.model.User;

public interface UserCustomRepository {

	User findUserById(Long id);
	User saveUser(User save);
	User findByEmail(String email);
	User findUserByEmailAndPassword(String email, String password);
	User findByToken(String token);

}
