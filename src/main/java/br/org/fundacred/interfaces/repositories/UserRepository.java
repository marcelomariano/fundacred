package br.org.fundacred.interfaces.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.org.fundacred.interfaces.custom.repositories.UserCustomRepository;
import br.org.fundacred.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long>, UserCustomRepository {

	@Query
	Optional<User> findById(Long id);

}