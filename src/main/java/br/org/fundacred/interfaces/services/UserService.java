package br.org.fundacred.interfaces.services;

import java.util.List;
import java.util.Optional;

import br.org.fundacred.model.User;

public interface UserService {

	Optional<User> findUser(Long id);
	List<User> findAll();
	User createUser(User user);
	User findByEmail(String email);
	User findUserByEmailAndPassword(String email, String password);
	User findByToken(String token);
	Optional<User> findById(Long id);

}