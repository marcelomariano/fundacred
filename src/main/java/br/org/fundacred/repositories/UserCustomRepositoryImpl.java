package br.org.fundacred.repositories;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Repository;

import br.org.fundacred.interfaces.custom.repositories.UserCustomRepository;
import br.org.fundacred.model.User;

@Repository
public class UserCustomRepositoryImpl implements UserCustomRepository {

	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional
	public User findUserById(Long id) {
		Session session = (Session) em.getDelegate();
		Criteria criteria = session.createCriteria(User.class, "u");
		criteria.createAlias("u.phones", "p", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.idEq(id));
		return (User) criteria.uniqueResult();
	}

	@Override
	public User saveUser(User user) {
		Session session = (Session) em.getDelegate();
		session.save(user);
		session.flush();
		return user;
	}

	@Override
	public User findByEmail(String email) {
		Session session = (Session) em.getDelegate();
		Criteria criteria = session.createCriteria(User.class, "u");
		criteria.add(Restrictions.eqProperty("u.email", email));
		return (User) criteria.uniqueResult();
	}

	@Override
	public User findUserByEmailAndPassword(String email, String password) {
		Session session = (Session) em.getDelegate();
		Criteria criteria = session.createCriteria(User.class, "u");
		criteria.add(Restrictions.eqProperty("u.email", email));
		criteria.add(Restrictions.eqProperty("u.password", password));
		return (User) criteria.uniqueResult();
	}

	@Override
	public User findByToken(String token) {
		Session session = (Session) em.getDelegate();
		Criteria criteria = session.createCriteria(User.class, "u");
		criteria.add(Restrictions.eqProperty("u.token", token));
		return (User) criteria.uniqueResult();
	}
}
