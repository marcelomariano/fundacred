package com.fundacred;

import java.time.Instant;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import br.org.fundacred.FundacredApplication;
import br.org.fundacred.business.interfaces.UserBusiness;
import br.org.fundacred.interfaces.repositories.UserRepository;
import br.org.fundacred.model.Phone;
import br.org.fundacred.model.User;
import br.org.fundacred.services.UserServiceImpl;
import br.org.fundacred.utis.UtilCodecBasic;

@ExtendWith(MockitoExtension.class)
class FundacredApplicationTests {

	@InjectMocks private UserServiceImpl userService;
	@Mock private UserRepository userRepository;
	@Mock private UserBusiness userBusiness;

	private User user;

	@BeforeEach
	@SuppressWarnings("unchecked")
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Instant now = Instant.now();
		@SuppressWarnings("rawtypes")
		Set<Phone> phones = new HashSet();
		phones.add(Phone.builder().ddd("051").number("99999999").build());
		user = User.builder().name("fundacred.user").email("fundacred.user@fundacred.org")
		.password(UtilCodecBasic.encodeBase64String("123")).phones(phones).created(now)
		.modified(now).token(UUID.randomUUID().toString()).lastLogin(now).build();
	}

	@Test
	void shouldCreatTest() {
		Mockito.lenient().when(userService.createUser(user)).thenReturn(user);
		Assertions.assertNotNull(user);
		Assertions.assertNotNull(user.getCreated());
	}

	@Test
	void shouldfoundUserByEmail() {
		Mockito.lenient().when(userService.findByEmail(user.getEmail())).thenReturn(user);
		Assertions.assertNotNull(user);
		Assertions.assertNotNull(user.getCreated());
	}

	@Test
	void sholdNotfoundUserByEmail() {
		User user = User.builder().email("fundacred.user@fundacred.org").build();
		Mockito.lenient().when(userService.findByEmail(this.user.getEmail())).thenReturn(user);
		Assertions.assertNull(user.getCreated());
	}

	@Test
	void fileLoadNullPointerException() {
		Optional<User> user = userService.findById(0L);
		Assertions.assertEquals(user, Optional.empty());
	}

}
